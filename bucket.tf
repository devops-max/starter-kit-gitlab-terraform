resource "aws_s3_bucket" "b" {
  bucket = var.BUCKET_NAME

  tags = {
    Team        = "DevOps"
  }
}
