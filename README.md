# Starter Kit GitLab Terraform

This is a starter kit for provisioning AWS resources using:

* Terraform 
* GitLab CI/CD
* GitLab Terraform Backend

You only need to add .gitlab-ci.yml file to your project  
in order to start provisioning from your existing Terraform  
template files to development and production environments.
