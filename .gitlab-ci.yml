# PREREQUISITES: set CICD vars: AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION, TF_VAR_BUCKET_NAME.
# Validate job runs on any branch, plan and deploy run on development, staging, and production branches.

# Run pipelines on commits to branches, but not on tags/etc to prevent state file issue
workflow:
  rules: 
    - if: $CI_COMMIT_BRANCH

image:
  name: terraform:latest
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

# Default output file for Terraform plan
variables:
  PLAN: plan.tfplan

cache:
  paths:
    - .terraform

before_script:
    # Set up state file per environment
  - export GITLAB_TF_ADDRESS="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_ENVIRONMENT_SLUG}"
  - echo $GITLAB_TF_ADDRESS
    # Append bucket name with the environment name
  - export TF_VAR_BUCKET_NAME="$TF_VAR_BUCKET_NAME-${CI_ENVIRONMENT_SLUG}"
    # Write terraform http backend definition to a file as it cannot be a var
  - echo -e "terraform { \n  backend \"http\" {}\n}" >> http-backend.tf
  - terraform --version
  - |
    terraform init -reconfigure \
    -backend-config="username=GITLAB_TF_PROJECT_ACCESS_TOKEN" \
    -backend-config="password=${GITLAB_TF_PROJECT_ACCESS_TOKEN}" \
    -backend-config="address=${GITLAB_TF_ADDRESS}" \
    -backend-config="lock_address=${GITLAB_TF_ADDRESS}/lock" \
    -backend-config="unlock_address=${GITLAB_TF_ADDRESS}/unlock"

stages:
  - validate
  - plan
  - apply
  - destroy

validate:
  stage: validate
  script: terraform validate

.plan_tpl:
  stage: plan
  dependencies: [validate]
  script: terraform plan -lock=false -out=$PLAN
  rules:
    - if: $CI_COMMIT_BRANCH == 'development'
      when: on_success
    - if: $CI_COMMIT_BRANCH == 'staging'
      when: on_success
    - if: $CI_COMMIT_BRANCH == 'production'
      when: on_success
    - when: never
  artifacts:
    name: plan
    expose_as: plan
    paths:
      - $PLAN

.apply_tpl:
  stage: apply
  script: terraform apply -lock=false -input=false $PLAN
  rules:
    - if: $CI_COMMIT_BRANCH == 'development'
      when: manual
    - if: $CI_COMMIT_BRANCH == 'staging'
      when: manual
    - if: $CI_COMMIT_BRANCH == 'production'
      when: manual
    - when: never

.destroy_tpl:
  stage: destroy
  script: terraform destroy -lock=false -auto-approve
  rules:
    - if: $DESTROY_JOBS_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH == 'development'
      when: manual
      allow_failure: true
    - if: $CI_COMMIT_BRANCH == 'staging'
      when: manual
      allow_failure: true
    - if: $CI_COMMIT_BRANCH == 'production'
      when: manual
    - when: never

plan_primary:
  extends: [ .plan_tpl]
  environment:
    name: primary-${CI_COMMIT_BRANCH}
    action: prepare

apply_primary:
  extends: [ .apply_tpl ]
  environment: 
    name: primary-${CI_COMMIT_BRANCH}
    on_stop: destroy_primary
  needs: [plan_primary]

destroy_primary:
  extends: [ .destroy_tpl  ]
  environment: 
    name: primary-${CI_COMMIT_BRANCH}
    action: stop
  needs: [apply_primary, plan_primary]

# plan_secondary:
#   extends: [ .plan_tpl]
#   environment:
#     name: secondary-${CI_COMMIT_BRANCH}
#     action: prepare

# apply_secondary:
#   extends: [ .apply_tpl ]
#   environment: 
#     name: secondary-${CI_COMMIT_BRANCH}
#     on_stop: destroy_secondary
#   needs: [plan_secondary]

# destroy_secondary:
#   extends: [ .destroy_tpl ]
#   environment: 
#     name: secondary-${CI_COMMIT_BRANCH}
#     action: stop
#   needs: [apply_secondary, plan_secondary]

